let input = document.querySelector('input');

document.addEventListener('DOMContentLoaded', () => {
  chrome.storage.sync.get({url: 'https://gitlab.com/rsrickshaw/Custom-New-Tab'}, _ => {
    input.value = _.url;
  });
});

input.addEventListener('input', () => {
  chrome.storage.sync.set({url: input.value});
});
