# The Problem
This extension is designed to address a specific quirk in Chrome: When changing the new tab page using a Chrome extension, the focus automatically goes to the address bar instead of the page. For some new tab pages this isn't an issue, but if your new tab page has an input box (perhaps a custom search handler) which you want to use instead of Chrome's address bar, it can be very annoying to have to click or repeatedly press tab to move the focus.

This appears to be an intentional design choice, as per [Our Benefactors at Google](https://developer.chrome.com/docs/extensions/develop/ui/override-chrome-pages):

> Remember that new tabs give keyboard focus to the address bar first. Don't rely on keyboard focus defaulting to other parts of the page.

# The Solution
To the web developer, some possible solutions may come to mind, such as using the `autocomplete` attribute on the page's input box or the `.focus()` method in JavaScript. Unfortunately, none of these will work (I already tried them in vain). *As far as I can tell,* the only way to escape the oppression of Chrome's address bar is to navigate to a different page, which is exactly what this extension does.

By telling Chrome to use a tiny redirect stub as the "real" new tab page, and then immediately redirecting to the page of our choice, we can guarantee that the focus will always be on the page and not the address bar. This has the added benefit of allowing us to use existing websites without having to turn them into new extensions.

# Installation
1. Clone or download this project.
2. Open the Chrome Extensions page and enable Developer Mode.
3. Click "Load unpacked" and choose the project folder.

# Usage
In the extensions menu, click on the Custom New Tab extension and enter the desired URL for the new tab page. That's it!

# "I installed and ended up on this page..."
This page is the default URL. See above.

# Other Notes
- Manifest V3 compatible
- The only permission used is the "storage" permission to store the specified new tab URL.
- I wanted to keep the extension as small as possible, and got it down 32 LOC for the entire codebase (without minification).
